import { AbstractControl} from '@angular/forms';
import { ValidatorFn } from '@angular/forms';

export const emailValidator = (): ValidatorFn => {
  return(control: AbstractControl): { [key: string]: string } => {
    const result = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/.test(control.value);
    return result === true ? null : {error: 'Wrong email type'};
  };
}

export const phoneValidator = (): ValidatorFn => {
  return(control: AbstractControl): { [key: string]: string } => {
    const result = /^\+84\d{9,10}$/.test(control.value);
    return result === true ? null : {error: 'Wrong phone number format'};
  };
}

export const ageValidator = (): ValidatorFn => {
  return(control: AbstractControl): { [key: string]: string } => {
    const result = control.value;
    return result > 18 ? null : {error: 'Wrong phone number format'};
  };
}
