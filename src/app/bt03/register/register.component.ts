import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ageValidator, emailValidator, phoneValidator} from '../Validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  countries = [
    'Vietnam',
    'Cambodia',
    'Laos',
    'Thailand',
    'Singapore'
  ];
  accountForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.createForm();
  }
  register() {
    console.log('done');
  }
  passwordValidator(group: FormGroup) {
    const pass = group.controls.password.value;
    const rePass = group.controls.rePassword.value;
    return pass === rePass ? null : {error: 'Password not match'};
  }
  createForm() {
    this.accountForm = this.formBuilder.group({
      email: ['', [Validators.required, emailValidator()]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      rePassword: ['', [Validators.required]],
      country: [this.countries[0], [Validators.required]],
      age: ['', [Validators.required, ageValidator()]],
      gender: ['', [Validators.required]],
      phone: ['', [Validators.required, phoneValidator()]]
    }, {validators: this.passwordValidator});
  }
  ngOnInit() {

  }

}
