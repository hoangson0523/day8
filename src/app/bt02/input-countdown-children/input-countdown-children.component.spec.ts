import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCountdownChildrenComponent } from './input-countdown-children.component';

describe('CountdownComponent', () => {
  let component: InputCountdownChildrenComponent;
  let fixture: ComponentFixture<InputCountdownChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCountdownChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCountdownChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
