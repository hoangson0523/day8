import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputCountdownChildrenComponent } from './output-countdown-children.component';

describe('OutputCountdownChildrenComponent', () => {
  let component: OutputCountdownChildrenComponent;
  let fixture: ComponentFixture<OutputCountdownChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputCountdownChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputCountdownChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
