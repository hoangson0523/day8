import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-output-countdown-children',
  templateUrl: './output-countdown-children.component.html',
  styleUrls: ['./output-countdown-children.component.css']
})
export class OutputCountdownChildrenComponent implements OnInit {
  @Output() setTime = new EventEmitter<number>();
  countdownForm: FormGroup;
  seconds = 0;

  getTime() {
    this.seconds = this.countdownForm.get('time').value;
  }

  sendToParent(seconds: number) {
    this.setTime.emit(seconds);
  }

  ngOnInit() {
    this.countdownForm = new FormGroup({
      time: new FormControl()
    });
  }

}
