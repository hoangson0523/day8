import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-count-down',
  templateUrl: './input-countdown-parent.component.html',
  styleUrls: ['./input-countdown-parent.component.css']
})
export class InputCountdownParentComponent implements OnInit {
  countdownForm: FormGroup;
  seconds = 0;
  getTime() {
    this.seconds = this.countdownForm.get('time').value;
  }

  ngOnInit() {
    this.countdownForm = new FormGroup({
      time: new FormControl()
    });
  }

}
