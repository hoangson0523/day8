import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCountdownParentComponent } from './input-countdown-parent.component';

describe('CountDownComponent', () => {
  let component: InputCountdownParentComponent;
  let fixture: ComponentFixture<InputCountdownParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCountdownParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCountdownParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
