import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-output-countdown-parent',
  templateUrl: './output-countdown-parent.component.html',
  styleUrls: ['./output-countdown-parent.component.css']
})
export class OutputCountdownParentComponent {
  seconds: number;
  minutes = 0;
  hours = 0;
  result = '';

  getTime(seconds: number) {
    this.seconds = seconds;
    this.count();
  }

  count() {
    if (this.seconds % 60 !== 0) {
      this.minutes = Math.floor(this.seconds / 60);
      this.seconds = this.seconds % 60;
    }
    if (this.minutes % 60 !== 0) {
      this.hours = Math.floor(this.minutes / 60);
      this.minutes = this.minutes % 60;
    }
    const countdown = setInterval(() => {
      this.seconds--;
      if (this.seconds <= 0 && this.minutes > 0) {
        this.seconds += 59;
        this.minutes -= 1;
      }
      if (this.minutes <= 0 && this.hours > 0) {
        this.minutes += 59;
        this.hours -= 1;
      }
      if (this.seconds === 0) {
        clearInterval(countdown);
        this.result = 'Time up!';
      }
    }, 1000);
  }

}
