import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputCountdownParentComponent } from './output-countdown-parent.component';

describe('OutputCountdownParentComponent', () => {
  let component: OutputCountdownParentComponent;
  let fixture: ComponentFixture<OutputCountdownParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputCountdownParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputCountdownParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
