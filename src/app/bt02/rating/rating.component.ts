import { Component, OnInit } from '@angular/core';
import {retry} from 'rxjs/operators';

interface Unit {
  value: number;
  active: boolean;
}

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  maxPoint = 10;
  input: Array<Unit> = [];
  ratingValue = this.maxPoint;

  setPoint(value) {
    this.ratingValue = value;
  }

  reset() {
    this.input.forEach(item => {
      item.active = false;
    });
    this.changeColor(this.ratingValue);
  }

  range() {
    this.input = Array.from({length: this.maxPoint}, (v, i) => ({
      value: i + 1,
      active: false
    }));
    return this.input;
  }

  changeColor(index) {
    this.input.forEach(item => {
      item.active = item.value <= index;
    });
  }
  constructor() { }

  ngOnInit() {
    this.range();
    this.changeColor(this.ratingValue);
  }

}
