import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FontSizeInputComponent } from './bt01/font-size-input/font-size-input.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PetComponent } from './bt01/pet/pet.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CalculatorComponent } from './bt01/calculator/calculator.component';
import { ColorPickerComponent } from './bt01/color-picker/color-picker.component';
import { Bt01Component } from './bt01/bt01.component';
import { Bt02Component } from './bt02/bt02.component';
import { InputCountdownParentComponent } from './bt02/input-countdown-parent/input-countdown-parent.component';
import { RatingComponent } from './bt02/rating/rating.component';
import { InputCountdownChildrenComponent } from './bt02/input-countdown-children/input-countdown-children.component';
import { OutputCountdownParentComponent } from './bt02/output-countdown-parent/output-countdown-parent.component';
import { OutputCountdownChildrenComponent } from './bt02/output-countdown-children/output-countdown-children.component';
import { Bt03Component } from './bt03/bt03.component';
import { RegisterComponent } from './bt03/register/register.component';
import { LoginComponent } from './bt03/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    FontSizeInputComponent,
    PetComponent,
    HomeComponent,
    CalculatorComponent,
    ColorPickerComponent,
    Bt01Component,
    Bt02Component,
    InputCountdownParentComponent,
    RatingComponent,
    InputCountdownChildrenComponent,
    OutputCountdownParentComponent,
    OutputCountdownChildrenComponent,
    Bt03Component,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'bt01',
        component: Bt01Component,
        children: [
          {path: 'fontSize', component: FontSizeInputComponent},
          {path: 'pet', component: PetComponent},
          {path: 'calculator', component: CalculatorComponent},
          {path: 'colorPicker', component: ColorPickerComponent}
        ]
      },
      {path: 'bt02',
        component: Bt02Component,
        children: [
          {path: 'countdown-input', component: InputCountdownParentComponent},
          {path: 'countdown-output', component: OutputCountdownParentComponent},
          {path: 'rating', component: RatingComponent}
        ]
      },
      {path: 'bt03',
        component: Bt03Component,
        children: [
          {path: 'register', component: RegisterComponent},
          {path: 'login', component: LoginComponent}
        ]
      }
    ]),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
