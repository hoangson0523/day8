import {Component, NgModule, OnInit} from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-font-size-input',
  templateUrl: './font-size-input.component.html',
  styleUrls: ['./font-size-input.component.css']
})
@NgModule({
  imports: [
    FormsModule
  ]
})
export class FontSizeInputComponent {
  fontSize = 14;
  changeSize(fontSize) {
    if (fontSize.value === null) {
      this.fontSize = 14;
    } else {
      this.fontSize = fontSize.value;
    }

  }
}
