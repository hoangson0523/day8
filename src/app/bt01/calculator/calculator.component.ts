import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {
  num1 = 0;
  num2 = 0;
  result = 0;
  select = 1;
  setNum1(value) {
    this.num1 = Number(value);
  }

  setNum2(value) {
    this.num2 = Number(value);
  }

  setSelect(value) {
    this.select = value;
  }

  calculate(select) {
    if (select === '1') {
      this.add();
    }
    if (select === '2') {
      this.minus();
    }
    if (select === '3') {
      this.multiply();
    }
    if (select === '4') {
      this.divide();
    }
  }

  add() {
    this.result = this.num1 + this.num2;
    console.log('add');
  }
  minus() {
    this.result = this.num1 - this.num2;
  }
  multiply() {
    this.result = this.num1 * this.num2;
  }
  divide() {
    this.result = this.num1 / this.num2;
  }
}
