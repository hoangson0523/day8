import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css']
})
export class PetComponent {
  petName = 'Not my cat';
  petImg = 'https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/' +
    '02-cat-training-NationalGeographic_1484324.jpg';
  setPetName(name) {
    if (name.value === null) {
      this.petName = 'Not my cat';
    } else {
      this.petName = name.value;
    }

  }
  setPetImg(img) {
    if (img.value === null) {
      this.petImg = 'https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/' +
        '02-cat-training-NationalGeographic_1484324.jpg';
    } else {
      this.petImg = img.value;
    }
  }
}
